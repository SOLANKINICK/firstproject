//importing index.js

const fs=require('fs');
//var jsonObj = require('./index1.js');

// //converting csv files to json array of objest
exports.useMatchPath = '../data/matches.csv';
exports.useDeliPath = '../data/deliveries.csv';

// //function to calculate matches played per season
exports.totCnt = function seasonCount(matchObj){
  var hashObj = matchObj.reduce((resultObj,currObj)=>{
    if(resultObj.hasOwnProperty(currObj.season))
      resultObj[currObj.season]++;
    else 
    resultObj[currObj.season] = 1;
    return resultObj;
  },{})
  var hashArr = []
  for(let keys in hashObj){
    hashArr.push([keys,hashObj[keys]]);
  }
  return hashArr;
}

exports.winTeam = function winningTeam(matchObj){
  var winnerObj = matchObj.reduce((resultObj,currObj)=>{
    if(resultObj.hasOwnProperty(currObj.winner)){
      if(resultObj[currObj.winner].hasOwnProperty(currObj.season)){
        resultObj[currObj.winner][currObj.season]++
      }
      else{
        resultObj[currObj.winner][currObj.season]=1
      }
    }
      else{
        resultObj[currObj.winner]={}
        resultObj[currObj.winner][currObj.season]=1
      }
      return resultObj
  },{})
  return winnerObj
}



exports.extraConcededRun = function extraRun(matchObj,deliObj,year){
  // var conceded ={};
  var matchId = matchObj.filter((obj) =>{
    if(obj.season == year){
      return obj
    }
  }).map((obj)=>{
    return obj.id
  });
  var conceded = deliObj.reduce((result,currDeli)=>{
    if(matchId.indexOf(currDeli.match_id)!=-1){
      if(result.hasOwnProperty(currDeli.bowling_team)){
        result[currDeli.bowling_team]+=currDeli.extra_runs
      }
      else{
        result[currDeli.bowling_team]=currDeli.extra_runs
      }
    }
    return result

  },{})

  let con = []
  for(let keys in conceded){
      con.push([keys,conceded[keys]])
  }
  return con
}

exports.economicalBowlers = function econBowl(matchObj,deliObj,year){
  var matchId15 = matchObj.filter((obj) =>{
    if(obj.season == year){
      return obj
    }
  }).map((obj)=>{
    return obj.id
  })
  
  var bowler = deliObj.reduce((result,currObj)=>{
    if(matchId15.indexOf(currObj.match_id)!=-1){
      if(!result.hasOwnProperty(currObj.bowler)){
        result[currObj.bowler]={};
        result[currObj.bowler]['boll']=0;
        result[currObj.bowler]['total_runs']=0;
      }
      if(currObj.wide_runs-currObj.noball_runs==0){
        result[currObj.bowler]['boll']++;
      }
      result[currObj.bowler]['total_runs']+=
              currObj.total_runs-currObj.bye_runs-currObj.legbye_runs;
    }
    return result
  },{})
              

  
  for(let keys in bowler){
    bowler[keys]['boll']= (bowler[keys]['boll']/6).toFixed(2);
    bowler[keys]['eco']=bowler[keys]['total_runs']/bowler[keys]['boll'];
  }
  var sortBowler = [];
  for (var keys in bowler) {
    sortBowler.push([keys, bowler[keys]['eco']]);
  }
  sortBowler.sort(function(a, b) {
    return a[1] - b[1];
  });

  var top = sortBowler.slice(0,10);//var to store top 10 bowlers with minimum economical count
  return top;
}


// function to calculate teams who won toss and won the match in same match
exports.tossAndWinMatch = function tossMatch(matchObj){
  var tossWin = matchObj.reduce((resultObj,currObj)=>{
    if(!resultObj.hasOwnProperty(currObj.team1)){
      resultObj[currObj.team1]=0;
    }
    if(!resultObj.hasOwnProperty(currObj.team2)){
      resultObj[currObj.team2]=0;
    }
    if(currObj.toss_winner == currObj.winner){
      resultObj[currObj.winner]++;
    }
    return resultObj;

  },{})
  return tossWin;
}

exports.playerOfMatch = function highPlayer(matchObj){
  let len=matchObj.length;
  let player={};
  let max=0;
  let current_season = matchObj[0].season;
  let playerseason={};
  let highplay=[];
  for(let matchIndex=0;matchIndex<len;matchIndex++){
    if(matchObj[matchIndex].season == current_season){
      if(player.hasOwnProperty(matchObj[matchIndex].player_of_match)){
        player[matchObj[matchIndex].player_of_match]++;
      }
      else {
      player[matchObj[matchIndex].player_of_match]=1;
      }
      if( player[matchObj[matchIndex].player_of_match] > max) {
        mxplayer = matchObj[matchIndex].player_of_match;
        max=player[matchObj[matchIndex].player_of_match];
      }
    }
    else {
      playerseason[current_season] = mxplayer;
      highplay.push([current_season+mxplayer,max])
      current_season=matchObj[matchIndex].season;
      matchIndex--;
      for(keys in player){
        delete player[keys];
      }
      max=0;
    }
  }
  return highplay;
}

exports.viratstrikerate = function virustrike(matchObj,deliObj){

  var matchidSeason = matchObj.reduce(function(resultArr,current_element){
    resultArr[current_element.id]=current_element.season;
    return resultArr
  },{})

  var currseason=matchidSeason[deliObj[0].match_id];
  var boll=0,totRun=0;
  var resultArr=[];
  var current_element={};
  for(let i=0;i<deliObj.length;i++){
    current_obj = deliObj[i];
    if(matchidSeason[current_obj.match_id] == currseason){
    if(current_obj.batsman=='V Kohli'){
      if(current_obj.wide_runs==0 )
      boll++;
      if(current_obj.noball_runs!=0) 
      boll++;
      totRun+=Number(current_obj.total_runs);
      }
    }
    else{
    if(totRun>0 && boll>0)
    strikerate = (totRun/boll)*100;
    else strikerate=0;
    resultArr.push([currseason,strikerate])
    currseason=matchidSeason[current_obj.match_id];
    boll=0;
    totRun=0;
    i--;
    }
    if(i==deliObj.length-1){
      if(totRun>0 && boll>0)
    strikerate = (totRun/boll)*100;
    else strikerate=0;
    resultArr.push([currseason,strikerate])
    currseason=matchidSeason[current_obj.match_id];
    boll=0;
    totRun=0;

    }
  }
  resultArr.sort()
  return resultArr;
}

exports.bestEconomyBowler = function superOver(deliObj){
  var resultObj = deliObj.reduce((result,currEle)=>{
    if(Number(currEle.is_super_over)>0){
      let bowler=currEle.bowler;
      if(!result.hasOwnProperty(bowler)){
        result[bowler]={};
        result[bowler]['boll']=0;
        result[bowler]['totRun']=0;
      }
      if(currEle.wide_runs==0 && currEle.noball_runs==0){
        result[bowler]['boll']++;  
      }
      let Run = currEle.legbye_runs-currEle.bye_runs;
      result[bowler]['totRun']+=currEle.total_runs-Run;
    }
    return result;
  },{})
  let minEconomy=-1;
  let economyPlayer;
  for(keys in resultObj){
    var economy  = (resultObj[keys]['totRun']/resultObj[keys]['boll'])*6;
    if(minEconomy == -1){
      minEconomy=economy;
      economyPlayer=keys; 
    }
    else if(economy<minEconomy){
      minEconomy=economy;
      economyPlayer=keys;
    }
    
  }
  return economyPlayer;
}





