const http = require('http')
const fs = require('fs')
const url = require('url')
http.createServer(function(req,res){
    const query = url.parse(req.url,true)
    //console.log("hi"+query.pathname)
    if(query.pathname == '/'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./index2.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else {
                res.write(data)
                res.end()
            }
        })
    }

    else if(query.pathname ==='/teamPerYearWin.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/teamPerYearWin.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }

    else if(query.pathname ==='/teamPerYearWin.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./teamPerYearWin.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }


    else if(query.pathname == '/extraConceded.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/extraConceded.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })

    }
    else if(query.pathname == '/matchesPerYear.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/matchesPerYear.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/economicalBowlers.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/economicalBowlers.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/tossandwin.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/tossandwin.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/economicalBowlers.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./economicalBowlers.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/extraConceded.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./extraConceded.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/matchesPerYear.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./matchesPerYear.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/tossandwin.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./tossandwin.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/highplayer.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./highplayer.html',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }


    else if(query.pathname == '/highPlayerOfmatch.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/highPlayerOfmatch.json','utf-8',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/viratstrikerate.html'){
        res.writeHead(200,{'content-type' : 'text/html'})
        fs.readFile('./viratstrikerate.html','utf-8',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }
    else if(query.pathname == '/viratstrike.json'){
        res.writeHead(200,{'content-type' : 'text/json'})
        fs.readFile('../output/viratstrike.json',(err,data)=>{
            if(err){
                res.end("Sorry!file does not exist")
            }
            else{
                res.write(data)
                res.end()
            }
        })
    }

    else{
        res.end("Wrong route!! put a correct path")
    }
    
    
    
    

}).listen(2313)