const csv=require('csvtojson')
var Ipl = require('./ipl.js');
const csvFilePath = Ipl.useMatchPath; 
const csvFilePath1=Ipl.useDeliPath;
const fs = require('fs')
var Ipl = require('./ipl.js');

csv({
    checkType:true
})
.fromFile(csvFilePath)
.then((matchObj)=>{
    csv({
        checkType:true
    })
    .fromFile(csvFilePath1)
    .then((delObj)=>{
        var totalSeasonCount = Ipl.totCnt(matchObj);
        fs.writeFile('../output/matchesPerYear.json' ,JSON.stringify(totalSeasonCount), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });
        var winTeamPerYear = Ipl.winTeam(matchObj);
        fs.writeFile('../output/teamPerYearWin.json' ,JSON.stringify(winTeamPerYear), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });

        var conceded = Ipl.extraConcededRun(matchObj,delObj,'2016');
        fs.writeFile('../output/extraConceded.json' ,JSON.stringify(conceded), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });
        var economicalBowler = Ipl.economicalBowlers(matchObj,delObj,'2015');
        fs.writeFile('../output/economicalBowlers.json' ,JSON.stringify(economicalBowler), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });
        var tossWinner = Ipl.tossAndWinMatch(matchObj);
        fs.writeFile('../output/tossandwin.json' ,JSON.stringify(tossWinner), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });

        var highPlayerOfMatch = Ipl.playerOfMatch(matchObj);
        fs.writeFile('../output/highPlayerOfmatch.json' ,JSON.stringify(highPlayerOfMatch), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });

        var strikeRate = Ipl.viratstrikerate(matchObj,delObj);
        fs.writeFile('../output/viratstrike.json' ,JSON.stringify(strikeRate), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });
        var superOver = Ipl.bestEconomyBowler(delObj);
        fs.writeFile('../output/superover.json' ,JSON.stringify(superOver), (err) => {
            if (err) throw err;
            console.log('Data written to file');
        });

    })
})